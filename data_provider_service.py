import pymysql
from passlib.hash import sha256_crypt


class DataProviderService:
    def __init__(self):
        """
        :creates: a new instance of connection and cursor
        """
        host = 'mysql'
        port = 3306
        user = 'root'
        password = 'rootpassword'
        database = 'blog_flask'
        self.conn = pymysql.connect(host=host, port=port, user=user, password=password, db=database)
        self.cursor = self.conn.cursor()

    def add_post(self, title, content, author_id=21):
        sql = """insert into post (title, content, author_id) values (%s, %s, %s)"""
        # hard coded for author 1
        input_values = (title, content, int(author_id))

        try:
            self.cursor.execute(sql, input_values)
            self.conn.commit()
        except Exception as exc:
            print(exc)
            self.conn.rollback()
            print("rolled back")

        sql_new_post_id = "select id from post order by created desc limit 1"
        self.cursor.execute(sql_new_post_id)
        new_post = self.cursor.fetchone()
        # the cursor and connection are closed after the new post is retrieved
        # self.cursor.close()
        # self.conn.close()
        return new_post[0]

    def get_post(self, post_id=None, limit=None):

        all_posts = []

        if post_id is None:
            sql = "SELECT * FROM post order by created desc"
            self.cursor.execute(sql)
            all_posts = self.cursor.fetchall()
        else:
            sql = """Select * from post where id = %s"""

            input_values = (post_id,)
            self.cursor.execute(sql, input_values)
            all_posts = self.cursor.fetchone()

        # self.cursor.close()
        # self.conn.close()
        return all_posts

    def get_post_with_author(self, post_id=None, limit=None):

        all_posts = []

        if post_id is None:
            sql = """SELECT p.id, author_id, created, title, content, u.username FROM post as p inner join user_table as u on p.author_id = u.id  order by created desc"""
            self.cursor.execute(sql)
            all_posts = self.cursor.fetchall()
        else:
            sql = """SELECT p.id, author_id, created, title, content, u.username FROM post as p inner join user_table as u on p.author_id = u.id where p.id = %s order by created desc"""
            input_values = (post_id,)
            self.cursor.execute(sql, input_values)

            all_posts = self.cursor.fetchone()

        # self.cursor.close()
        # self.conn.close()
        return all_posts

    def update_post(self, post_id, new_post):
        updated_post = None
        current_post = DataProviderService().get_post(post_id=post_id)

        if current_post:
            sql = """update post set title = %s, content = %s where id = %s"""

            input_values = (new_post['title'], new_post['content'], post_id)

            try:
                self.cursor.execute(sql, input_values)
                self.conn.commit()
            except Exception as exc:
                self.conn.rollback()

            # updated_post = DataProviderService.get_post(post_id)
            updated_post = self.get_post(post_id)

        return updated_post


    def delete_post(self, post_id):
        if int(post_id) < 0:
            raise ValueError("Parameter [id] should be a positive number!")

        if post_id > 0:
            sql = """delete from post where id = %s"""
            input_values = (post_id,)

            try:
                self.cursor.execute(sql, input_values)
                self.conn.commit()
            except Exception as exc:
                self.conn.rollback()

            return True

        return False

    def is_user_valid(self, username, passwd):
        sql = """select id, username, password from user_table where username = %s"""

        input_values = (username,)
        valid_user = None
        try:
            self.cursor.execute(sql, input_values)
            valid_user = self.cursor.fetchone()
            # self.cursor.close()
            # self.conn.close()
        except Exception as exc:
            print(exc)
            self.conn.rollback()

        same_password = sha256_crypt.verify(passwd, valid_user[2])
        print(sha256_crypt.verify(passwd, valid_user[2]))
        if same_password:
            return valid_user
        else:
            return None

    def is_logged_in_user_valid(self, user_id, username):
        sql = """select id, username from user_table where id = %s and username = %s"""

        input_values = (user_id, username)
        valid_user = None
        try:
            self.cursor.execute(sql, input_values)
            valid_user = self.cursor.fetchone()
            self.cursor.close()
            self.conn.close()
        except Exception as exc:
            print(exc)
            self.conn.rollback()

        return valid_user

    def get_random_posts(self, limit=3):

        all_posts = []

        sql = "SELECT * FROM post limit %s"
        input_values = (limit,)
        self.cursor.execute(sql, input_values)
        all_posts = self.cursor.fetchall()

        # self.cursor.close()
        # self.conn.close()
        return all_posts

    def add_user(self, username, passwd):
        sql = """insert into user_table (username, password) values (%s, %s)"""

        hashed_pword = sha256_crypt.hash(passwd)
        input_values = (username, hashed_pword)

        try:
            self.cursor.execute(sql, input_values)
            self.conn.commit()
        except Exception as exc:
            self.conn.rollback()

        sql_new_user_id = "select id from user_table order by id desc limit 1"
        self.cursor.execute(sql_new_user_id)
        new_user = self.cursor.fetchone()
        # self.cursor.close()
        # self.conn.close()
        return new_user[0]


