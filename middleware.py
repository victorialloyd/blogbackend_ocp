from flask import jsonify
from flask import abort
from flask import make_response
from flask import request
from urllib.request import urlopen

from data_provider_service import DataProviderService

DATA_PROVIDER = DataProviderService()


def post():
    posts = DATA_PROVIDER.get_post_with_author()
    return jsonify({"posts": posts})


def post_by_id(post_id):
    current_post = DATA_PROVIDER.get_post_with_author(post_id)
    if current_post:
        return jsonify({"post": current_post})
    else:
        # In case we did not find the candidate by id
        # we send HTTP 404 - Not Found error to the client
        abort(404)


def delete_post(post_id):
    try:
        if DATA_PROVIDER.delete_post(post_id):
            return make_response("", 200)
        else:
            return make_response("", 404)
    except ValueError as err:
        err_response = make_response("", 500)
        return err_response


def update_post(post_id):
    title = request.form["title"]
    content = request.form["content"]
    formatted_title = (str(title)).title()
    formatted_content = (str(content))[:1].upper() + (str(content))[1:]
    new_post = {
        "title": formatted_title,
        "content": formatted_content
    }
    updated_post = DATA_PROVIDER.update_post(post_id, new_post)
    if not updated_post:
        return make_response('', 404)
    else:
        return jsonify({"post": updated_post})


# not being used
def random_posts(nr_of_items):
    posts = DATA_PROVIDER.get_random_posts(nr_of_items)
    return jsonify({"posts": posts, "total": len(posts)})


def add_post():
    try:
        title = request.form["title"]
        content = request.form["content"]
        user_id = request.form["userid"]
    except:
        user_id = 21 # currently hard-coded to user 21 - Guest

    formatted_title = (str(title)).title()
    formatted_content = (str(content))[:1].upper() + (str(content))[1:]
    # print(title, content, user_id)
    if user_id is None:
        user_id = 21

    new_post_id = DATA_PROVIDER.add_post(title=formatted_title, content=formatted_content, author_id=user_id)
    # print("New POST ID is :", new_post_id)
    created_post = DATA_PROVIDER.get_post_with_author(new_post_id)
    if created_post:
        return jsonify({"post": created_post})
    else:
        # In case we did not find the post by id
        # we send HTTP 404 - Not Found error to the client
        abort(404)


def login():
    username = request.form["username"]
    password = request.form["password"]
    user = DATA_PROVIDER.is_user_valid(username, password)
    if user is not None:
        resp = make_response(jsonify({'id': user[0],'username': user[1]}), 200)
        # resp.set_cookie('userid', str(user[0]))
        # resp.set_cookie('username', str(user[1]))
        return resp
    else:
        abort(404)


def logout():
    userid = request.cookies.get('userid')
    username = request.cookies.get('username')
    user = DATA_PROVIDER.is_logged_in_user_valid(userid, username)

    if user is not None:
        resp= make_response(jsonify({'id': '','username': ''}), 200)
        # resp.set_cookie('userid', '', expires=0)
        # resp.set_cookie('username', '', expires=0)
        return resp
    else:
        abort(404)


def register():
    username = request.form["username"]
    password = request.form["password"]

    new_user_id = DATA_PROVIDER.add_user(username, password)

    if new_user_id:
        return jsonify({"user": new_user_id})
    else:
        # In case we did not create the user
        # we send HTTP 404 - Not Found error to the client
        abort(404)


def get_github_users(search_term):
    with urlopen("https://api.github.com/search/users?q=" + search_term) as gitdata:
        content = gitdata.read()
    return content

def test_routes():
    return "hello everyone"
